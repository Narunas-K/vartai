/* Includes ------------------------------------------------------------------*/

#ifndef __USART_RADIO_H_
#define __USART_RADIO_H_

#include  "stm32f4xx_usart.h"  
#include <string.h>
#include "stdio.h"
#include "stdlib.h"

void usart_radio_init(void);
int8_t usart_radio_get(void);

void usart_radio_send(char * data);
void radio_send(char * dataptr, int length);

#endif  // __USART_RADIO_H_

/*****************END OF FILE****/

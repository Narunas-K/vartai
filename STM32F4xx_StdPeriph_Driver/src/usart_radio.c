#include "usart_radio.h"

// Private macro -------------------------------------------------------------
#ifdef __GNUC__
  // With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf set to 'Yes') calls __io_putchar()
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif // __GNUC__ 


void usart_radio_init()
{
	GPIO_InitTypeDef  GPIO_InitStructure1;
	USART_InitTypeDef USART_InitStructure1;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  
  /* Connect USART3 pins to AF2 */
	// RX = PA1 TX = PA0
  GPIO_InitStructure1.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_0;
	GPIO_InitStructure1.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure1.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure1.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure1.GPIO_PuPd = GPIO_PuPd_UP ;
	
	GPIO_Init(GPIOA, &GPIO_InitStructure1); 
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource1, GPIO_AF_UART4);

	GPIO_Init(GPIOA, &GPIO_InitStructure1); 
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource0, GPIO_AF_UART4);

  USART_InitStructure1.USART_BaudRate = 9600;
  USART_InitStructure1.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure1.USART_StopBits = USART_StopBits_1;
  USART_InitStructure1.USART_Parity = USART_Parity_No;
  USART_InitStructure1.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure1.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
          
  USART_Init(UART4, &USART_InitStructure1);
  USART_Cmd(UART4, ENABLE);
}

int8_t usart_radio_get()
{  
	return (uint8_t)USART_ReceiveData(UART4);
}

void usart_radio_send(char *data)     
{
  uint16_t length, i;
  length=strlen(data);
  for(i=0; i<length; i++)
  {
      USART_ClearFlag(UART4, USART_FLAG_TXE);
      USART_SendData(UART4, data[i]);
      while(USART_GetFlagStatus(UART4, USART_FLAG_TXE)==0){}
  }
}

//siai funkcijai reikia nurodyti kiek simboliu siunciame
void radio_send(char * dataptr, int length)
{
  char * data = dataptr;
  uint16_t i;
  for(i=0; i<length; i++)
  {
      USART_ClearFlag(UART4, USART_FLAG_TXE);
      USART_SendData(UART4, data[i]);
      while(USART_GetFlagStatus(UART4, USART_FLAG_TXE)==0){}
  }
}

//******************************************************************************
// Hosting of stdio functionality through UART4
//******************************************************************************

#include <rt_misc.h>

#pragma import(__use_no_semihosting_swi)

struct __FILE { int handle; /* Add whatever you need here */ };
FILE __stdout;
FILE __stdin;

//Arba PUTCHAR_PROTOTYPE arba fputc
PUTCHAR_PROTOTYPE
{
  // Place your implementation of fputc here
  // e.g. write a character to the USART
  USART_SendData(UART4, (uint8_t) ch);

  // Loop until the end of transmission
  while (USART_GetFlagStatus(UART4, USART_FLAG_TC) == RESET)
  {}

  return ch;
}
/*
int fputc(int ch, FILE *f)
{
	static int last;

	if ((ch == (int)'\n') && (last != (int)'\r'))
	{
		last = (int)'\r';
		while(USART_GetFlagStatus(UART4, USART_FLAG_TXE) == RESET);
		USART_SendData(UART4, last);
	} else
		last = ch;

	while(USART_GetFlagStatus(UART4, USART_FLAG_TXE) == RESET){};
  USART_SendData(UART4, ch);

  return(ch);
}
*/
int fgetc(FILE *f)
{
	char ch;

	while(USART_GetFlagStatus(UART4, USART_FLAG_RXNE) == RESET);

	ch = USART_ReceiveData(UART4);

  return((int)ch);
}

int ferror(FILE *f)
{
  /* Your implementation of ferror */
  return EOF;
}

void _ttywrch(int ch)
{
	static int last;

	if ((ch == (int)'\n') && (last != (int)'\r'))
	{
		last = (int)'\r';
		while(USART_GetFlagStatus(UART4, USART_FLAG_TXE) == RESET);
		USART_SendData(UART4, last);
	} else
		last = ch;

	while(USART_GetFlagStatus(UART4, USART_FLAG_TXE) == RESET){};

  USART_SendData(UART4, ch);
}

void _sys_exit(int return_code)
{
	label: goto label; /* endless loop */
}



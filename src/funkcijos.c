#ifndef _KINTAMIEJI_H_
#define _MAIN_H_
#include "funkcijos.h"
#include "kintamieji.h"
// Stabdymo funkcijos aprasymas
void StabdymasFunkcija(void){
		printf("STABDYMAS\n");
		GPIO_SetBits(GPIOC, AtidarymoRele); // isjungiam atidarymo rele
		GPIO_SetBits(GPIOC, UzdarymoRele);	// isjungiam uzdarymo rele
	SpindulysDaviklisJudesysBusena=GPIO_ReadInputDataBit(GPIOC, SpindulysDaviklisJudesys);
	if(SpindulysDaviklisJudesysBusena==ReleHIGH){
		GPIO_SetBits(GPIOC, SpindulysDaviklisJudesys);	// isjungiam ,,akies'' spinduli, jeigu neisjungtas
	}
		AtidarymoReleBusena=GPIO_ReadOutputDataBit(GPIOC, AtidarymoRele);
		UzdarymoReleBusena=GPIO_ReadOutputDataBit(GPIOC, UzdarymoRele);
		BluetoothKintamasis=0;
}
// Vartu uzdarymo proceduros pirmoji funkcija
void UzdarymasFunkcija(void){
		printf("2.3.0\n");
		GPIO_ResetBits(GPIOC, SpindulysDaviklisJudesys); // Ijungiama spindulio rele
		Delay(1000); // tam , kad spetu isijungt spindulio rele
		DaviklisAtidarytaBusena=GPIO_ReadInputDataBit(GPIOA, DaviklisAtidaryta);
		DaviklisUzdarytaBusena=GPIO_ReadInputDataBit(GPIOA, DaviklisUzdaryta);
		DaviklisJudesysBusena=GPIO_ReadInputDataBit(GPIOA, DaviklisJudesys);
		printf("DaviklisAtidarytaBusena %d\n", DaviklisAtidarytaBusena);
		printf("DaviklisUzdarytaBusena %d\n", DaviklisUzdarytaBusena);
		printf("DaviklisJudesysBusena %d\n", DaviklisJudesysBusena);
	if(DaviklisJudesysBusena==LOW){
			GPIO_SetBits(GPIOC, SpindulysDaviklisJudesys); // Isjungiama spindulio rele
			AtidarymasFunkcija();
	}
	else if(DaviklisUzdarytaBusena==HIGH){
		printf("2.3.1\n");
		printf("VARTAI JAU YRA UZDARYTI");
		Delay(10);
	}
	else if(DaviklisAtidarytaBusena==HIGH){
		printf("2.3.2\n");
		VartaiUzdaromi();// Vartu uzdarymo proceduros antroji funkcija
	}
	else if((DaviklisUzdarytaBusena==LOW)&&(DaviklisAtidarytaBusena==LOW)){
		printf("2.3.3\n");
		VartaiUzdaromi();
	}
	
}
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! kas auksciau reik perziuret nes negerai gali buti su LOW ir HIGH(PERZIURETA 2015 08 15 21:24)
//Vartu uzdarymo proceduros antroji funkcija
void VartaiUzdaromi(void){
		printf("2.4.0\n");
		AtidarymoReleBusena=GPIO_ReadOutputDataBit(GPIOC, AtidarymoRele);
		UzdarymoReleBusena=GPIO_ReadOutputDataBit(GPIOC, UzdarymoRele);
	
 if((AtidarymoReleBusena==ReleLOW)&&(UzdarymoReleBusena==ReleHIGH)){//cia isjungta atidarymo rele ir ijungta uzdarymo
		printf("2.4.1\n");
		GPIO_SetBits(GPIOC,AtidarymoRele);//dar karta isjungiam Atidarymo rele
		GPIO_ResetBits(GPIOC, UzdarymoRele);//dar karta ijungiam Uzdarymo Rele
	}
	else if((AtidarymoReleBusena==ReleLOW)&&(UzdarymoReleBusena==ReleLOW)){// cia atidarymo ir uzdarymo reles isjungtos
		printf("2.4.2\n");
		GPIO_SetBits(GPIOC, AtidarymoRele);// isjungiu atidarymo rele
		GPIO_ResetBits(GPIOC, UzdarymoRele); // ijungiu uzdarymo rele
	}
	else if((AtidarymoReleBusena==ReleHIGH)&&(UzdarymoReleBusena==ReleLOW)){// ijungta atidarymo rele ir isjungta uzdarymo rele
		printf("2.4.3\n");
		GPIO_SetBits(GPIOC, AtidarymoRele);//isjungiama atidarymo rele
		GPIO_SetBits(GPIOC, UzdarymoRele);// isjungiama uzdarymo rele
		GPIO_ResetBits(GPIOC, UzdarymoRele);// ijungiama atidarymo rele
	}
	else if((AtidarymoReleBusena==ReleHIGH)&&(UzdarymoReleBusena==ReleHIGH)){//JEI TAIP ATSITIKTU TAI NEGERAI KAZKAS, BET JUST IN CASE ijungta atidarymo ir uzdarymo reles
		printf("2.4.4 LABAI NEGERAI\n");
		GPIO_SetBits(GPIOC, AtidarymoRele);// isjungiama atidarymo rele
		GPIO_SetBits(GPIOC, UzdarymoRele);// isjungiama atidarymo rele
		GPIO_ResetBits(GPIOC, UzdarymoRele); // ijungiama uzdarymo rele
	}
	else{// blogiausiu atveju, kas manau nelabai imanoma
		printf("2.4.5 LABAI NEGERAI\n");
		GPIO_SetBits(GPIOC, AtidarymoRele);// isjungiama atidarymo rele
		GPIO_SetBits(GPIOC, UzdarymoRele);// isjungiama atidarymo rele
		GPIO_ResetBits(GPIOC, UzdarymoRele); // ijungiama uzdarymo rele
	}
	
	UzdarymasDavikliai();
}
// Davikliu funkcija uzdarymo funkcijai
void UzdarymasDavikliai(void){
		printf("2.5.0\n");
		UzdarymoReleBusena=GPIO_ReadOutputDataBit(GPIOC, UzdarymoRele);
		SpindulysDaviklisJudesysBusena=GPIO_ReadOutputDataBit(GPIOC, SpindulysDaviklisJudesys);
		if(SpindulysDaviklisJudesysBusena==ReleLOW){
		GPIO_ResetBits(GPIOC, SpindulysDaviklisJudesys); // ijungiam ,,Akies'' spinduli, jeigu jis isjungtas
		}
	while(UzdarymoReleBusena==ReleHIGH){// vyks tol, kol bus ijungta rele
		printf("2.5.1\n");
		USART_ClearFlag(USART2, USART_FLAG_RXNE);
	while (USART_GetFlagStatus(USART2, USART_FLAG_RXNE)==RESET){
		DaviklisUzdarytaBusena=GPIO_ReadInputDataBit(GPIOA, DaviklisUzdaryta);
		StabdymasLaukiuMAtidarymas=GPIO_ReadInputDataBit(GPIOA, MygtukasAtidaryti);
		StabdymasLaukiuMUzdarymas=GPIO_ReadInputDataBit(GPIOA, MygtukasUzdaryti);
		DaviklisJudesysBusena=GPIO_ReadInputDataBit(GPIOA, DaviklisJudesys);
		printf("Bluetooth ciklas ,Uzdarymas'\n");
	if(DaviklisUzdarytaBusena==HIGH||StabdymasLaukiuMAtidarymas==HIGH||StabdymasLaukiuMUzdarymas==HIGH||DaviklisJudesysBusena==LOW){
		printf("Iseinu is bluetooth ciklo DaviklisUzdarytaBusena=%d StabdymasLaukiuMAtidarymas%d StabdymasLaukiuMUzdarymas%d DaviklisJudesysBusena%d\n",DaviklisUzdarytaBusena, StabdymasLaukiuMAtidarymas, StabdymasLaukiuMUzdarymas, DaviklisJudesysBusena);
				break;
			}
		}
	if(USART_GetFlagStatus(USART2, USART_FLAG_RXNE)==SET){
		BluetoothKintamasis=USART_ReceiveData(USART2);
	}
			USART_ClearFlag(USART2, USART_FLAG_RXNE);
	if(DaviklisUzdarytaBusena==HIGH||StabdymasLaukiuMAtidarymas==HIGH||StabdymasLaukiuMUzdarymas==HIGH||BluetoothKintamasis==48||BluetoothKintamasis==49){// jei vienas is triju mygtuku buna HIGH vartai stabdomi
		printf("2.5.2\n");
		StabdymasFunkcija();
		Delay(1000);// Delay'ius tam, kad vartai nepradetu atsidarinet/uzsidarinet, kai tik yra sustabdomi
	}
	else if(DaviklisJudesysBusena==LOW){
		printf("2.5.3\n");
		StabdymasFunkcija();
		Delay(1000);
		AtidarymasFunkcija();
	
	}
	}
}



///////////////////////////////////////////////////ATIDARYMO DALIS
// Vartu uzdarymo proceduros pirmoji funkcija
void AtidarymasFunkcija(void){
		printf("3.3.0\n");
		DaviklisAtidarytaBusena=GPIO_ReadInputDataBit(GPIOA, DaviklisAtidaryta);
		DaviklisUzdarytaBusena=GPIO_ReadInputDataBit(GPIOA, DaviklisUzdaryta);
		printf("DaviklisAtidarytaBusena %d\n", DaviklisAtidarytaBusena);
		printf("DaviklisUzdarytaBusena %d\n", DaviklisUzdarytaBusena);
	if(DaviklisAtidarytaBusena==HIGH){
		printf("3.3.1");
		printf("VARTAI JAU YRA ATIDARYTI\n");
		Delay(10);
	}
	else if(DaviklisUzdarytaBusena==HIGH){
		printf("3.3.2\n");
		VartaiAtidaromi();// Vartu atidarymo proceduros antroji funkcija
	}
	else if((DaviklisAtidarytaBusena==LOW)&&(DaviklisUzdarytaBusena==LOW)){
		printf("3.3.3\n");
		VartaiAtidaromi();
	}
}
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! kas auksciau reik perziuret nes negerai gali buti su LOW ir HIGH(PERZIURETA 2015 08 15 21:24)
//Vartu uzdarymo proceduros antroji funkcija
void VartaiAtidaromi(void){
		printf("3.4.0\n");
		AtidarymoReleBusena=GPIO_ReadOutputDataBit(GPIOC, AtidarymoRele); //sios dvi eilutes nuskaito reliu busenas
		UzdarymoReleBusena=GPIO_ReadOutputDataBit(GPIOC, UzdarymoRele);
	if((UzdarymoReleBusena==ReleLOW)&&(AtidarymoReleBusena==ReleHIGH)){//cia isjungta uzdarymo rele ir ijungta atidarymo
		printf("3.4.1\n");
		GPIO_SetBits(GPIOC,UzdarymoRele);//dar karta isjungiam Uzdarymo rele
		GPIO_ResetBits(GPIOC, AtidarymoRele);//dar karta ijungiam Atidarymo Rele
	}
	else if((AtidarymoReleBusena==ReleLOW)&&(UzdarymoReleBusena==ReleLOW)){// cia atidarymo ir uzdarymo reles isjungtos
		printf("3.4.2\n");
		GPIO_SetBits(GPIOC, UzdarymoRele);// isjungiu uzdarymo rele
		GPIO_ResetBits(GPIOC, AtidarymoRele); // ijungiu atidarymo rele
	}
	else if((UzdarymoReleBusena==ReleHIGH)&&(AtidarymoReleBusena==ReleLOW)){// ijungta uzdarymo rele ir isjungta atidarymo rele
		printf("3.4.3\n");
		GPIO_SetBits(GPIOC,UzdarymoRele);//isjungiama atidarymo rele
		GPIO_SetBits(GPIOC, AtidarymoRele);// isjungiama uzdarymo rele
		GPIO_ResetBits(GPIOC, AtidarymoRele);// ijungiama atidarymo rele
	}
	else if((AtidarymoReleBusena==ReleHIGH)&&(UzdarymoReleBusena==ReleHIGH)){//JEI TAIP ATSITIKTU TAI NEGERAI KAZKAS, BET JUST IN CAS ijungta atidarymo ir uzdarymo reles
		printf("3.4.4 LABAI NEGERAI\n");
		GPIO_SetBits(GPIOC, AtidarymoRele);// isjungiama atidarymo rele
		GPIO_SetBits(GPIOC, UzdarymoRele);// isjungiama atidarymo rele
		GPIO_ResetBits(GPIOC, AtidarymoRele); // ijungiama atidarymoo rele
	}
	else{// blogiausiu atveju, kas ,manau, nelabai imanoma
		printf("3.4.5 LABAI NEGERAI\n");
		GPIO_SetBits(GPIOC, AtidarymoRele);// isjungiama atidarymo rele
		GPIO_SetBits(GPIOC, UzdarymoRele);// isjungiama uzdarymo rele
		GPIO_ResetBits(GPIOC, AtidarymoRele); // ijungiama atidarymo rele
	}
	
	AtidarymasDavikliai();
}
// Davikliu funkcija uzdarymo funkcijai
void AtidarymasDavikliai(void){
		printf("3.5.0\n");
		AtidarymoReleBusena=GPIO_ReadOutputDataBit(GPIOC, AtidarymoRele); //nuskaitau atidarymo reles busena
	while(AtidarymoReleBusena==ReleHIGH){// vyks tol, kol bus ijungta rele
		printf("3.5.1\n");
		USART_ClearFlag(USART2, USART_FLAG_RXNE);
	while (USART_GetFlagStatus(USART2, USART_FLAG_RXNE)==RESET){
		DaviklisAtidarytaBusena=GPIO_ReadInputDataBit(GPIOA, DaviklisAtidaryta);
		StabdymasLaukiuMAtidarymas=GPIO_ReadInputDataBit(GPIOA, MygtukasAtidaryti);
		StabdymasLaukiuMUzdarymas=GPIO_ReadInputDataBit(GPIOA, MygtukasUzdaryti);
		printf("Bluetooth 'atidarymas davikliai ciklas\n'");
	if(DaviklisAtidarytaBusena==HIGH||StabdymasLaukiuMAtidarymas==HIGH||StabdymasLaukiuMUzdarymas==HIGH){
		printf("Iseinu is bluetooth 'atidarymas davikliai\n'");
		break;
		}
		}
	if(USART_GetFlagStatus(USART2, USART_FLAG_RXNE)==SET){
		BluetoothKintamasis=USART_ReceiveData(USART2);
	}
		USART_ClearFlag(USART2, USART_FLAG_RXNE);

	if(DaviklisAtidarytaBusena==HIGH||StabdymasLaukiuMAtidarymas==HIGH||StabdymasLaukiuMUzdarymas==HIGH||BluetoothKintamasis==48||BluetoothKintamasis==49){// jei vienas is triju mygtuku buna HIGH vartai stabdomi
		printf("3.5.2\n");
		StabdymasFunkcija();
		Delay(1000);// Delay'ius tam, kad vartai nepradetu atsidarinet/uzsidarinet, kai tik yra sustabdomi
	}
	}
}

/////////////////reikalinga DELAY funkcijai *PRADZIA*///////////////////////
volatile uint32_t msTicks;                      /* counts 1ms timeTicks       */
/*----------------------------------------------------------------------------
  SysTick_Handler
 *----------------------------------------------------------------------------*/
void SysTick_Handler(void) {
  msTicks++;
}
 /*----------------------------------------------------------------------------
  delays number of tick Systicks (happens every 1 ms)
 *----------------------------------------------------------------------------*/
void Delay (uint32_t dlyTicks) {                                              
  uint32_t curTicks;

  curTicks = msTicks;
  while ((msTicks - curTicks) < dlyTicks);
}
/////////////////reikalinga DELAY funkcijai *PABAIGA*///////////////////////


#endif //_KINTAMIEJI_H_


// 2016 02 23 17:57 Padaryta, kad viektu su akimi(lazeriu ir fotorezistoriumi) . Kolkas jokiu problemu nepastebeta @N.K

#include "stm32f4xx.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_usart.h"
#include "stdio.h"
#include <stdint.h>
#include "string.h"
#include "usart_radio.h"

GPIO_InitTypeDef  GPIO_InitStructure; 	
USART_InitTypeDef USART_InitStructure;

//PIN'ai
const uint16_t AtidarymoRele = GPIO_Pin_1; // Atidarymo reles pinas PC1
const uint16_t UzdarymoRele = GPIO_Pin_2; // Uzdarymo reles pinas PC2
const uint16_t MygtukasAtidaryti=GPIO_Pin_4; // Atidarymo mygtuko pinas PA4
const uint16_t MygtukasUzdaryti=GPIO_Pin_5; // Uzdarymo mygtuko pinas PA5
const uint16_t DaviklisAtidaryta=GPIO_Pin_8; //Daviklio ,,DaviklisAtidaryta'' pinas PA8
const uint16_t DaviklisUzdaryta=GPIO_Pin_10; // Daviklio ,,DaviklisUzdaryta'' pinas PA10
const uint16_t SpindulysDaviklisJudesys=GPIO_Pin_0;// Lazerio spindulio ,,SpindulysDaviklisJudesys'' pinas PC0


//kintamieji
//uint8_t currentButtonStatus=0; 
uint8_t AtidarymasLaukiu=0; //MygtukasAtidaryti kintamasis, kuriame sauguma mygtuko busena
uint8_t UzdarymasLaukiu=0; // MygtukasUzdaryti kintamasis, kuriame saugoma mygtuko busena
uint8_t AtidarytaLaukiu=0; // DaviklisAtidaryta kintamasis, kuriame saugoma mygtuko busena
uint8_t DaviklisJudesysBusena=0; //DaviklisJudesysLaukiu, kuriame saugoma lazerio ir fotorezistoriaus busena 
uint8_t AtidarymasLaukiuBluetooth=0;//kintamasis, kuriame saugoma is bluetooth gauta info
uint8_t UzdarymasLaukiuBluetooth=0; // kintamasis, kuriame saugoma is bluetooth gauta info

uint8_t DaviklisAtidarytaLaukiu=0; //  DaviklisAtidarytaLaukiu kintamasis, kuriame saugoma daviklio busena
uint8_t DaviklisUzdarytaLaukiu=0; //  DaviklisUzdarytaLaukiu kintamasis, kuriame saugoma daviklio busena
uint8_t Keitimas=0; // kintamasis kad galima butu padaryti su case'ais
uint8_t StabdymasLaukiuMAtidarymas=0;// stabdymo kintamasis, kad galima butu sustabdyti su atdidarymo mygtuku
uint8_t StabdymasLaukiuMUzdarymas=0;//stabdymo kintamasis, kad galima butu sustabdyti su uzdarymo mygtuku

uint8_t StabdymasLaukiuBluetoothAtidarymas=0;//stabdymo kintamasis, kad galima butu sustabdyti per bluetooth
uint8_t StabdymasLaukiuBluetoothUzdarymas=0;// stabdymo kintamasis, kad galima butu sustabdyti per bluetootj

uint8_t AtidarymoReleBusena; // kintamasis atidarymo reles busenai nustatyti.
uint8_t UzdarymoReleBusena; //kintamasis uzdarymo reles busenai nustatyti



uint8_t SpindulysDaviklisJudesysBusena; // kintamasis spindulio relei
uint16_t SpindulysVertePries=0; //kintamasis foto rezistoriaus vertei nuskaityti pries ijungiant lazerio spinduli
uint16_t SpindulysVertePo=0; // kintamasis fotorezistoriaus vertei nuskaityi po lazerio ijungimo




uint8_t DaviklisAtidarytaBusena;// kintamasis atidarymo daviklio busenai nustatyti
uint8_t DaviklisUzdarytaBusena; // kintamasis uzdarymo daviklio busenai nustatyti
const uint8_t HIGH=1; // mygtuko padeciai nustatyti(mygtukas nuspaustas)
const uint8_t LOW=0; //mygtyko padeciai nustatyti(mygtukas neispaustas)
const uint8_t ReleHIGH=0; // reles padeciai nustatyti ar ji ijungta, ar isjungta (cia rele ijungta)
const uint8_t ReleLOW=1;//reles padeciai nustatyti ar ji ijungta, ar isjungta (cia rele isjungta)
uint8_t keitimas=0; // tam, kad galima butu padaryti su case'ais
uint8_t BluetoothKintamasis=0;

//funkciju header'iai
void Init_Reles(void);// inicializuoju reliu pinus, kaip output;
void Init_MygtukaiDavikliai(void);//(funkcijos headeris) inicializuoju mygtuku ir davikliu pinus kaip input
void init_gpio(void);// inicializuojami uart4 pin'ai skirti komunikacijai per ftdi232 rl su kompu
void init_usart(void);//  inicializuojamas uart4 protokolas skirtas komunikacijai su kompu
void init_gpio_bluetooth(void);//inicializuojami usart2 pin'ai skirti bluetooth komunikacijai
void init_usart_bluetooth(void);// inicializuojamas usart2 protokolas skirtas bluetooth komunikacijai
void StabdymasFunkcija(void); // stabdymo funkcija
void UzdarymasFunkcija(void); // vartu uzdarymo proceduros pirmoji funkcija
void VartaiUzdaromi(void); // vartu uzdarymo proceduros antroji funkcija
void UzdarymasDavikliai(void);// Davikliu funkcija uzdarymo funkcijai
void AtidarymasFunkcija(void); // vartu atidarymo proceduros pirmoji funkcija
void VartaiAtidaromi(void); // vartu atidarymo proceduros antroji funkcija
void AtidarymasDavikliai(void);// Davikliu funkcija atidarymo funkcijai
uint8_t Bluetooth_Gauti(void); // funkcija reikalinga informacijai is usart2 per bluetooth gauti
void delay(uint32_t ms); // delay funkcijai (netobulas delay'ius, reiks normalaus)!!!!!!!!!
void adc_configure(void);//ADC konverterio, reikalingo ,,akiai'', inicializacija
int adc_convert(void); //ADC konverterio funkcija nuskaityti duomenis is pino


/////////////////reikalinga DELAY funkcijai *PRADZIA*///////////////////////
volatile uint32_t msTicks;                      /* counts 1ms timeTicks       */
/*----------------------------------------------------------------------------
  SysTick_Handler
 *----------------------------------------------------------------------------*/
void SysTick_Handler(void) {
  msTicks++;
}
 /*----------------------------------------------------------------------------
  delays number of tick Systicks (happens every 1 ms)
 *----------------------------------------------------------------------------*/
void Delay (uint32_t dlyTicks) {                                              
  uint32_t curTicks;

  curTicks = msTicks;
  while ((msTicks - curTicks) < dlyTicks);
}
/////////////////reikalinga DELAY funkcijai *PABAIGA*///////////////////////

	uint16_t rxbuf=2;


int main(void) {
	// Kad veikty delay'aus funkcija
	SystemCoreClockUpdate();                      /* Get Core Clock Frequency   */
  if (SysTick_Config(SystemCoreClock / 1000)) { /* SysTick 1 msec interrupts  */
    while (1);                                  /* Capture error              */
  }
	Init_Reles();//inicializuoju atidarymo ir uzdarymo reles
	Delay(50);
	Init_MygtukaiDavikliai(); // inicializuoju mygtukus ir daviklius
	Delay(50);
	init_gpio(); //inicializuoju uart4'o pin;us
	Delay(50);
	init_usart(); // inicializuoju uart4'a
	Delay(50);
	init_gpio_bluetooth();//inicializuoju usart2 pin'us
	Delay(50);
	init_usart_bluetooth();//inicializuoju usart2 
	Delay(50);
	adc_configure();// inicializuoju ADC
	Delay(50);
	GPIO_SetBits(GPIOC, AtidarymoRele);//tam, kad atidarymo ir uzdarymo reles perkrovus mikrovaldikli butu isjungtos
	Delay(50);
	GPIO_SetBits(GPIOC, UzdarymoRele);// tam, kad atidarymo ir uzdarymo reles perkrovus mikrovaldikli butu isjungtos
	Delay(50);
	GPIO_SetBits(GPIOC, SpindulysDaviklisJudesys);
	Delay(50);
	while (1) {
		
			printf("1.0\n");
			printf("Bluetooth kintamasis %d\n", BluetoothKintamasis);
			
			USART_ClearFlag(USART2, USART_FLAG_RXNE);
		
		while (USART_GetFlagStatus(USART2, USART_FLAG_RXNE)==RESET){
			AtidarymasLaukiu=GPIO_ReadInputDataBit(GPIOA, MygtukasAtidaryti);
			UzdarymasLaukiu=GPIO_ReadInputDataBit(GPIOA, MygtukasUzdaryti);
			DaviklisAtidarytaLaukiu=GPIO_ReadInputDataBit(GPIOA, DaviklisAtidaryta);
			DaviklisUzdarytaLaukiu=GPIO_ReadInputDataBit(GPIOA, DaviklisUzdaryta);
			BluetoothKintamasis=0;
			printf("Bluetooth laukimo metu nuskaitinejami kiti mygtukai\n");
		if(AtidarymasLaukiu==HIGH||UzdarymasLaukiu==HIGH||DaviklisAtidarytaLaukiu==HIGH||DaviklisUzdarytaLaukiu==HIGH){
			printf("Iseinama is bluetooth ciklo AtidarymasLaukiu=%d. UzdarymasLaukiu=%d DaviklisAtidarytaLaukiu=%d DaviklisUzdarytaLaukiu%d\n", AtidarymasLaukiu, UzdarymasLaukiu, DaviklisAtidarytaLaukiu, DaviklisUzdarytaLaukiu);
		break;
	}
		}
		if(USART_GetFlagStatus(USART2, USART_FLAG_RXNE)==SET){
			BluetoothKintamasis=USART_ReceiveData(USART2); // nuskaitom is blueetooth kintamojo reiksme
		}
			USART_ClearFlag(USART2, USART_FLAG_RXNE);
			printf("BluetoothKintamasis pradzioje %d\n", BluetoothKintamasis);
		
//jei nuspaudziam mygtuka ,,Atidaryti''	
	
	if(AtidarymasLaukiu==HIGH||BluetoothKintamasis==48){
		AtidarymoReleBusena=GPIO_ReadOutputDataBit(GPIOC, AtidarymoRele); // sios dvi eilutes reikalingos issiaiskinti reliu busenas
		UzdarymoReleBusena=GPIO_ReadOutputDataBit(GPIOC, UzdarymoRele);
		BluetoothKintamasis=0;
	if((AtidarymoReleBusena==ReleLOW)&&(UzdarymoReleBusena==ReleLOW)){ // tikrinu ar isjungtos atidarymo ir uzdarymo reles
		printf("1.1\n");
		Keitimas=1;// vartai bus atidaromi
		}
	else if(AtidarymoReleBusena==ReleHIGH || UzdarymoReleBusena==ReleHIGH){ // jei bent viena rele ijungta vartai yra stabdomi 
		printf("1.2\n");
		Keitimas=3;	// vartai bus stabdomi	
	}
	else{
		printf("1.3\n");
		Keitimas=0;}// default'ine reiksme
	}
	
//jei nuspaudziam mygtuka Uzdaryti
	
	else if(UzdarymasLaukiu==HIGH||BluetoothKintamasis==49){
		AtidarymoReleBusena=GPIO_ReadOutputDataBit(GPIOC, AtidarymoRele);//sios dvi eilutes reikalingos issiaiskinti reliu busenas
		UzdarymoReleBusena=GPIO_ReadOutputDataBit(GPIOC, UzdarymoRele);
		BluetoothKintamasis=0;
	if(UzdarymoReleBusena==ReleLOW &&AtidarymoReleBusena==ReleLOW){ //tikrinu ar isjungtos atidarymo ir uzdarymo reles
		printf("1.4\n");
		Keitimas=2;// vartai bus uzdaromi
	}	
	else if(UzdarymoReleBusena==ReleHIGH ||AtidarymoReleBusena==ReleHIGH){ // jei bent viena rele ijungta vartai bus stabdomi
		printf("1.5\n");// vartai bus stabdomi
		Keitimas=3;
	}
	else{
	printf("1.6\n");
		Keitimas=0;// default'ine reiksme 
	}
	
	}
	else{
		printf("1.7\n");
		Keitimas=0;//default'ine reiksme
	}
	//Cia nukreipiama kintamasis ,,Keitimas'' ir aprasomas ar vartai bus atidarinejami, uzdarinejami , stabdomi
	//kiekviename case'e yra idetas delay'ius vienai sekundei, kad zmogus spestu atleisti mygtuka ir del to neprasidetu keisti dalykai
	switch(Keitimas){
	case 1:
		printf("2.1\n");
		Delay(1000);
		AtidarymasFunkcija(); //Atidarymo funkcija
	break;
	
	case 2:
		printf("2.2\n");
		Delay(1000);
		UzdarymasFunkcija();// Uzdarymo funkcija
	break;
	
	case 3:
		printf("2.3\n");
		Delay(1000);
		StabdymasFunkcija();//Stabdymo funkcija
	break;
	
	case 0:
		printf("2.4\n");
	break;
	
	default:
		printf("2.5\n");
	break;
}
	printf("XXXXXXXXXXXXXXXXXXXXXX\n");
	
	
	
			} // loop'o pabaiga
}// main'o pabaiga
		
// Stabdymo funkcijos aprasymas
void StabdymasFunkcija(void){
		printf("STABDYMAS\n");
		GPIO_SetBits(GPIOC, AtidarymoRele); // isjungiam atidarymo rele
		GPIO_SetBits(GPIOC, UzdarymoRele);	// isjungiam uzdarymo rele
		SpindulysDaviklisJudesysBusena=GPIO_ReadInputDataBit(GPIOC, SpindulysDaviklisJudesys);
	if(SpindulysDaviklisJudesysBusena==ReleHIGH){
		GPIO_SetBits(GPIOC, SpindulysDaviklisJudesys);	// isjungiam ,,akies'' spinduli, jeigu neisjungtas
	}
		AtidarymoReleBusena=GPIO_ReadOutputDataBit(GPIOC, AtidarymoRele);
		UzdarymoReleBusena=GPIO_ReadOutputDataBit(GPIOC, UzdarymoRele);
		BluetoothKintamasis=0;
}
// Vartu uzdarymo proceduros pirmoji funkcija
void UzdarymasFunkcija(void){
		printf("2.3.0\n");
		SpindulysVertePries=adc_convert();
		Delay(10); // uzdelsimas, kad spetu nuskaityti verte
		GPIO_ResetBits(GPIOC, SpindulysDaviklisJudesys); // Ijungiama spindulio rele
		Delay(1000); // tam , kad spetu isijungt spindulio rele
		DaviklisAtidarytaBusena=GPIO_ReadInputDataBit(GPIOA, DaviklisAtidaryta);
		DaviklisUzdarytaBusena=GPIO_ReadInputDataBit(GPIOA, DaviklisUzdaryta);
		SpindulysVertePo=adc_convert();
	//DaviklisJudesysBusena=GPIO_ReadInputDataBit(GPIOA, DaviklisJudesys);
		printf("DaviklisAtidarytaBusena %d\n", DaviklisAtidarytaBusena);
		printf("DaviklisUzdarytaBusena %d\n", DaviklisUzdarytaBusena);
		printf("SpindulysVertePries %d\n", SpindulysVertePries);
		printf("SpindulysVertePo %d \n", SpindulysVertePo);
		if(SpindulysVertePries<50)
		{SpindulysVertePries=+50;}
	if((SpindulysVertePries>=SpindulysVertePo)){
			GPIO_SetBits(GPIOC, SpindulysDaviklisJudesys); // Isjungiama spindulio rele
			AtidarymasFunkcija();
	}
	else if(DaviklisUzdarytaBusena==HIGH){
		printf("2.3.1\n");
		printf("VARTAI JAU YRA UZDARYTI");
		Delay(10);
	}
	else if(DaviklisAtidarytaBusena==HIGH){
		printf("2.3.2\n");
		VartaiUzdaromi();// Vartu uzdarymo proceduros antroji funkcija
	}
	else if((DaviklisUzdarytaBusena==LOW)&&(DaviklisAtidarytaBusena==LOW)){
		printf("2.3.3\n");
		VartaiUzdaromi();
	}
	
}
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! kas auksciau reik perziuret nes negerai gali buti su LOW ir HIGH(PERZIURETA 2015 08 15 21:24)
//Vartu uzdarymo proceduros antroji funkcija
void VartaiUzdaromi(void){
		printf("2.4.0\n");
		AtidarymoReleBusena=GPIO_ReadOutputDataBit(GPIOC, AtidarymoRele);
		UzdarymoReleBusena=GPIO_ReadOutputDataBit(GPIOC, UzdarymoRele);
	
 if((AtidarymoReleBusena==ReleLOW)&&(UzdarymoReleBusena==ReleHIGH)){//cia isjungta atidarymo rele ir ijungta uzdarymo
		printf("2.4.1\n");
		GPIO_SetBits(GPIOC,AtidarymoRele);//dar karta isjungiam Atidarymo rele
		GPIO_ResetBits(GPIOC, UzdarymoRele);//dar karta ijungiam Uzdarymo Rele
	}
	else if((AtidarymoReleBusena==ReleLOW)&&(UzdarymoReleBusena==ReleLOW)){// cia atidarymo ir uzdarymo reles isjungtos
		printf("2.4.2\n");
		GPIO_SetBits(GPIOC, AtidarymoRele);// isjungiu atidarymo rele
		GPIO_ResetBits(GPIOC, UzdarymoRele); // ijungiu uzdarymo rele
	}
	else if((AtidarymoReleBusena==ReleHIGH)&&(UzdarymoReleBusena==ReleLOW)){// ijungta atidarymo rele ir isjungta uzdarymo rele
		printf("2.4.3\n");
		GPIO_SetBits(GPIOC, AtidarymoRele);//isjungiama atidarymo rele
		GPIO_SetBits(GPIOC, UzdarymoRele);// isjungiama uzdarymo rele
		GPIO_ResetBits(GPIOC, UzdarymoRele);// ijungiama atidarymo rele
	}
	else if((AtidarymoReleBusena==ReleHIGH)&&(UzdarymoReleBusena==ReleHIGH)){//JEI TAIP ATSITIKTU TAI NEGERAI KAZKAS, BET JUST IN CASE ijungta atidarymo ir uzdarymo reles
		printf("2.4.4 LABAI NEGERAI\n");
		GPIO_SetBits(GPIOC, AtidarymoRele);// isjungiama atidarymo rele
		GPIO_SetBits(GPIOC, UzdarymoRele);// isjungiama atidarymo rele
		GPIO_ResetBits(GPIOC, UzdarymoRele); // ijungiama uzdarymo rele
	}
	else{// blogiausiu atveju, kas manau nelabai imanoma
		printf("2.4.5 LABAI NEGERAI\n");
		GPIO_SetBits(GPIOC, AtidarymoRele);// isjungiama atidarymo rele
		GPIO_SetBits(GPIOC, UzdarymoRele);// isjungiama atidarymo rele
		GPIO_ResetBits(GPIOC, UzdarymoRele); // ijungiama uzdarymo rele
	}
	
	UzdarymasDavikliai();
}
// Davikliu funkcija uzdarymo funkcijai
void UzdarymasDavikliai(void){
		printf("2.5.0\n");
		UzdarymoReleBusena=GPIO_ReadOutputDataBit(GPIOC, UzdarymoRele);
		SpindulysDaviklisJudesysBusena=GPIO_ReadOutputDataBit(GPIOC, SpindulysDaviklisJudesys);
		SpindulysVertePries=adc_convert();
		if(SpindulysDaviklisJudesysBusena==ReleLOW){
		GPIO_ResetBits(GPIOC, SpindulysDaviklisJudesys); // ijungiam ,,Akies'' spinduli, jeigu jis isjungtas
		Delay(50);
		SpindulysVertePries=adc_convert();
		}
	while(UzdarymoReleBusena==ReleHIGH){// vyks tol, kol bus ijungta rele
		printf("2.5.1\n");
		USART_ClearFlag(USART2, USART_FLAG_RXNE);
	while (USART_GetFlagStatus(USART2, USART_FLAG_RXNE)==RESET){
		DaviklisUzdarytaBusena=GPIO_ReadInputDataBit(GPIOA, DaviklisUzdaryta);
		StabdymasLaukiuMAtidarymas=GPIO_ReadInputDataBit(GPIOA, MygtukasAtidaryti);
		StabdymasLaukiuMUzdarymas=GPIO_ReadInputDataBit(GPIOA, MygtukasUzdaryti);
		SpindulysVertePo=adc_convert();
		printf("Bluetooth ciklas ,Uzdarymas' SpindulysVertePries %d SpindulysVertePo %d\n", SpindulysVertePries, SpindulysVertePo);
	if(DaviklisUzdarytaBusena==HIGH||StabdymasLaukiuMAtidarymas==HIGH||StabdymasLaukiuMUzdarymas==HIGH||(SpindulysVertePo<=(SpindulysVertePries-40))){
		printf("Iseinu is bluetooth ciklo DaviklisUzdarytaBusena=%d StabdymasLaukiuMAtidarymas%d StabdymasLaukiuMUzdarymas%d SpindulysVertePries%d\n SpindulysVertePo %d\n",DaviklisUzdarytaBusena, StabdymasLaukiuMAtidarymas, StabdymasLaukiuMUzdarymas, SpindulysVertePries, SpindulysVertePo);
				break;
			}
		}
	if(USART_GetFlagStatus(USART2, USART_FLAG_RXNE)==SET){
		BluetoothKintamasis=USART_ReceiveData(USART2);
	}
			USART_ClearFlag(USART2, USART_FLAG_RXNE);
	if(DaviklisUzdarytaBusena==HIGH||StabdymasLaukiuMAtidarymas==HIGH||StabdymasLaukiuMUzdarymas==HIGH||BluetoothKintamasis==48||BluetoothKintamasis==49){// jei vienas is triju mygtuku buna HIGH vartai stabdomi
		printf("2.5.2\n");
		StabdymasFunkcija();
		Delay(1000);// Delay'ius tam, kad vartai nepradetu atsidarinet/uzsidarinet, kai tik yra sustabdomi
	}
	else if(DaviklisJudesysBusena==LOW){
		printf("2.5.3\n");
		StabdymasFunkcija();
		Delay(1000);
		AtidarymasFunkcija();
	
	}
	}
}



///////////////////////////////////////////////////ATIDARYMO DALIS
// Vartu uzdarymo proceduros pirmoji funkcija
void AtidarymasFunkcija(void){
		printf("3.3.0\n");
		DaviklisAtidarytaBusena=GPIO_ReadInputDataBit(GPIOA, DaviklisAtidaryta);
		DaviklisUzdarytaBusena=GPIO_ReadInputDataBit(GPIOA, DaviklisUzdaryta);
		printf("DaviklisAtidarytaBusena %d\n", DaviklisAtidarytaBusena);
		printf("DaviklisUzdarytaBusena %d\n", DaviklisUzdarytaBusena);
	if(DaviklisAtidarytaBusena==HIGH){
		printf("3.3.1");
		printf("VARTAI JAU YRA ATIDARYTI\n");
		Delay(10);
	}
	else if(DaviklisUzdarytaBusena==HIGH){
		printf("3.3.2\n");
		VartaiAtidaromi();// Vartu atidarymo proceduros antroji funkcija
	}
	else if((DaviklisAtidarytaBusena==LOW)&&(DaviklisUzdarytaBusena==LOW)){
		printf("3.3.3\n");
		VartaiAtidaromi();
	}
}
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! kas auksciau reik perziuret nes negerai gali buti su LOW ir HIGH(PERZIURETA 2015 08 15 21:24)
//Vartu uzdarymo proceduros antroji funkcija
void VartaiAtidaromi(void){
		printf("3.4.0\n");
		AtidarymoReleBusena=GPIO_ReadOutputDataBit(GPIOC, AtidarymoRele); //sios dvi eilutes nuskaito reliu busenas
		UzdarymoReleBusena=GPIO_ReadOutputDataBit(GPIOC, UzdarymoRele);
	if((UzdarymoReleBusena==ReleLOW)&&(AtidarymoReleBusena==ReleHIGH)){//cia isjungta uzdarymo rele ir ijungta atidarymo
		printf("3.4.1\n");
		GPIO_SetBits(GPIOC,UzdarymoRele);//dar karta isjungiam Uzdarymo rele
		GPIO_ResetBits(GPIOC, AtidarymoRele);//dar karta ijungiam Atidarymo Rele
	}
	else if((AtidarymoReleBusena==ReleLOW)&&(UzdarymoReleBusena==ReleLOW)){// cia atidarymo ir uzdarymo reles isjungtos
		printf("3.4.2\n");
		GPIO_SetBits(GPIOC, UzdarymoRele);// isjungiu uzdarymo rele
		GPIO_ResetBits(GPIOC, AtidarymoRele); // ijungiu atidarymo rele
	}
	else if((UzdarymoReleBusena==ReleHIGH)&&(AtidarymoReleBusena==ReleLOW)){// ijungta uzdarymo rele ir isjungta atidarymo rele
		printf("3.4.3\n");
		GPIO_SetBits(GPIOC,UzdarymoRele);//isjungiama atidarymo rele
		GPIO_SetBits(GPIOC, AtidarymoRele);// isjungiama uzdarymo rele
		GPIO_ResetBits(GPIOC, AtidarymoRele);// ijungiama atidarymo rele
	}
	else if((AtidarymoReleBusena==ReleHIGH)&&(UzdarymoReleBusena==ReleHIGH)){//JEI TAIP ATSITIKTU TAI NEGERAI KAZKAS, BET JUST IN CAS ijungta atidarymo ir uzdarymo reles
		printf("3.4.4 LABAI NEGERAI\n");
		GPIO_SetBits(GPIOC, AtidarymoRele);// isjungiama atidarymo rele
		GPIO_SetBits(GPIOC, UzdarymoRele);// isjungiama atidarymo rele
		GPIO_ResetBits(GPIOC, AtidarymoRele); // ijungiama atidarymoo rele
	}
	else{// blogiausiu atveju, kas ,manau, nelabai imanoma
		printf("3.4.5 LABAI NEGERAI\n");
		GPIO_SetBits(GPIOC, AtidarymoRele);// isjungiama atidarymo rele
		GPIO_SetBits(GPIOC, UzdarymoRele);// isjungiama uzdarymo rele
		GPIO_ResetBits(GPIOC, AtidarymoRele); // ijungiama atidarymo rele
	}
	
	AtidarymasDavikliai();
}
// Davikliu funkcija uzdarymo funkcijai
void AtidarymasDavikliai(void){
		printf("3.5.0\n");
		AtidarymoReleBusena=GPIO_ReadOutputDataBit(GPIOC, AtidarymoRele); //nuskaitau atidarymo reles busena
	while(AtidarymoReleBusena==ReleHIGH){// vyks tol, kol bus ijungta rele
		printf("3.5.1\n");
		USART_ClearFlag(USART2, USART_FLAG_RXNE);
	while (USART_GetFlagStatus(USART2, USART_FLAG_RXNE)==RESET){
		DaviklisAtidarytaBusena=GPIO_ReadInputDataBit(GPIOA, DaviklisAtidaryta);
		StabdymasLaukiuMAtidarymas=GPIO_ReadInputDataBit(GPIOA, MygtukasAtidaryti);
		StabdymasLaukiuMUzdarymas=GPIO_ReadInputDataBit(GPIOA, MygtukasUzdaryti);
		printf("Bluetooth 'atidarymas davikliai ciklas\n'");
	if(DaviklisAtidarytaBusena==HIGH||StabdymasLaukiuMAtidarymas==HIGH||StabdymasLaukiuMUzdarymas==HIGH){
		printf("Iseinu is bluetooth 'atidarymas davikliai\n'");
		break;
		}
		}
	if(USART_GetFlagStatus(USART2, USART_FLAG_RXNE)==SET){
		BluetoothKintamasis=USART_ReceiveData(USART2);
	}
		USART_ClearFlag(USART2, USART_FLAG_RXNE);

	if(DaviklisAtidarytaBusena==HIGH||StabdymasLaukiuMAtidarymas==HIGH||StabdymasLaukiuMUzdarymas==HIGH||BluetoothKintamasis==48||BluetoothKintamasis==49){// jei vienas is triju mygtuku buna HIGH vartai stabdomi
		printf("3.5.2\n");
		StabdymasFunkcija();
		Delay(1000);// Delay'ius tam, kad vartai nepradetu atsidarinet/uzsidarinet, kai tik yra sustabdomi
	}
	}
}


void Init_Reles(void) {
    GPIO_InitTypeDef GPIO_InitStruct;		// These structures are defined for every peripheral so every peripheral has it's own TypeDef.
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);	// This enables the peripheral clock to the GPIOD IO module, Every peripheral's clock has to be enabled
		GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;			// we want the pins to be an output
    GPIO_InitStruct.GPIO_Pin = AtidarymoRele|UzdarymoRele|SpindulysDaviklisJudesys ;	// we want to configure all LED GPIO pins	
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz; 	// this sets the GPIO modules clock speed
		GPIO_InitStruct.GPIO_OType = GPIO_OType_PP; 	// this sets the pin type to push / pull (as opposed to open drain)
		GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL; 	// this sets the pullup / pulldown resistors to be inactive
		GPIO_Init(GPIOC, &GPIO_InitStruct); 			// this finally passes all the values to the GPIO_Init function which takes care of setting the corresponding bits.
}
/*

uint8_t Bluetooth_Gauti(void){
	uint8_t bufferis;		
	USART_ClearFlag(USART2, USART_FLAG_RXNE);
	//while (USART_GetFlagStatus(USART2, USART_FLAG_RXNE)==RESET){}
	bufferis=USART_ReceiveData(USART2);
	//while (USART_GetFlagStatus(USART2, USART_FLAG_RXNE)==RESET){}
		USART_ClearFlag(USART2, USART_FLAG_RXNE);
return(bufferis);
}
*/

void Init_MygtukaiDavikliai(void) {
		GPIO_InitTypeDef GPIO_InitStruct;
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	
		GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStruct.GPIO_Pin = MygtukasAtidaryti|MygtukasUzdaryti|DaviklisAtidaryta|DaviklisUzdaryta;
		GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_Init(GPIOA, &GPIO_InitStruct); 
}
void init_gpio_bluetooth(void){
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	// RX = PA2 TX = PA3
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2  | GPIO_Pin_3;	   			
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;			
	      
	GPIO_Init(GPIOA, &GPIO_InitStructure); 				
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);     
	GPIO_Init(GPIOA, &GPIO_InitStructure); 
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);
}

void init_usart_bluetooth(void)
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);	 
           
	USART_InitStructure.USART_BaudRate = 9600;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;	
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
          
  USART_Init(USART2, &USART_InitStructure);		  
  USART_Cmd(USART2, ENABLE);
}

void init_gpio(void){
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	// RX = PA1 TX = PA0
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0  | GPIO_Pin_1;	   			
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;			
	      
	GPIO_Init(GPIOA, &GPIO_InitStructure); 				
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource0, GPIO_AF_UART4);     
	GPIO_Init(GPIOA, &GPIO_InitStructure); 
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource1, GPIO_AF_UART4);
}

void init_usart(void)
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE);	 
           
	USART_InitStructure.USART_BaudRate = 9600;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;	
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
          
  USART_Init(UART4, &USART_InitStructure);		  
  USART_Cmd(UART4, ENABLE);
}

void adc_configure(){
 ADC_InitTypeDef ADC_init_structure; //Structure for adc confguration
 GPIO_InitTypeDef GPIO_initStructre; //Structure for analog input pin
 //Clock configuration
 RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC2,ENABLE);//The ADC1 is connected the APB2 peripheral bus thus we will use its clock source
 RCC_AHB1PeriphClockCmd(RCC_AHB1ENR_GPIOCEN,ENABLE);//Clock for the ADC port!! Do not forget about this one ;)
 //Analog pin configuration
 GPIO_initStructre.GPIO_Pin = GPIO_Pin_5;//The channel 10 is connected to PC0 mano channel 9 connected to PB1
 GPIO_initStructre.GPIO_Mode = GPIO_Mode_AN; //The PC0 pin is configured in analog mode
 GPIO_initStructre.GPIO_PuPd = GPIO_PuPd_NOPULL; //We don't need any pull up or pull down
 GPIO_Init(GPIOC,&GPIO_initStructre);//Affecting the port with the initialization structure configuration
 //ADC structure configuration
 ADC_DeInit();
 ADC_init_structure.ADC_DataAlign = ADC_DataAlign_Right;//data converted will be shifted to right
 ADC_init_structure.ADC_Resolution = ADC_Resolution_10b;//Input voltage is converted into a 12bit number giving a maximum value of 4096
 ADC_init_structure.ADC_ContinuousConvMode = ENABLE; //the conversion is continuous, the input data is converted more than once
 ADC_init_structure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1;// conversion is synchronous with TIM1 and CC1 (actually I'm not sure about this one :/)
 ADC_init_structure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;//no trigger for conversion
 ADC_init_structure.ADC_NbrOfConversion = 1;//I think this one is clear :p
 ADC_init_structure.ADC_ScanConvMode = DISABLE;//The scan is configured in one channel
 ADC_Init(ADC2,&ADC_init_structure);//Initialize ADC with the previous configuration
 //Enable ADC conversion
 ADC_Cmd(ADC2,ENABLE);
 //Select the channel to be read from
 ADC_RegularChannelConfig(ADC2,ADC_Channel_15,1,ADC_SampleTime_144Cycles);
}
int adc_convert(){
 ADC_SoftwareStartConv(ADC2);//Start the conversion
 while(!ADC_GetFlagStatus(ADC2, ADC_FLAG_EOC));//Processing the conversion
 return ADC_GetConversionValue(ADC2); //Return the converted data
}




The project code is for home automatic gate system.
The system is based on STM32F4 Discovery development board. The system can be controlled over bluetooth or by standart microcontroller buttons pad.
Also, the system includes safety feature which stop gates from opening/closing when the safety laser beam is broke.
Hardware of teh system consits of:
-STM32 F4 discovery dev board.
-220V relay module
-380V relay module
-380V electric motor
-Button pad
-HC-05 bluetooth module
-RS232 uart module
-Laser 
-Photodiodes for reading laser beam signal
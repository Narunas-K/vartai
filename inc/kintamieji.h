#ifndef _KINTAMIEJI_H_
#define _KINTAMIEJI_H_
#include "stm32f4xx.h"

GPIO_InitTypeDef  GPIO_InitStructure; 	
USART_InitTypeDef USART_InitStructure;

//PIN'ai
const uint16_t AtidarymoRele = GPIO_Pin_1; // Atidarymo reles pinas PC1
const uint16_t UzdarymoRele = GPIO_Pin_2; // Uzdarymo reles pinas PC2
const uint16_t MygtukasAtidaryti=GPIO_Pin_4; // Atidarymo mygtuko pinas PA4
const uint16_t MygtukasUzdaryti=GPIO_Pin_5; // Uzdarymo mygtuko pinas PA5
const uint16_t DaviklisAtidaryta=GPIO_Pin_8; //Daviklio ,,DaviklisAtidaryta'' pinas PA8
const uint16_t DaviklisUzdaryta=GPIO_Pin_10; // Daviklio ,,DaviklisUzdaryta'' pinas PA10
const uint16_t DaviklisJudesys=GPIO_Pin_15; // Daviklio ,,DaviklisJudesys'' pinas PA15 
const uint16_t SpindulysDaviklisJudesys=GPIO_Pin_0;// Lazerio spindulio ,,SpindulysDaviklisJudesys'' pinas PC0


//kintamieji
//uint8_t currentButtonStatus=0; 
uint8_t AtidarymasLaukiu=0; //MygtukasAtidaryti kintamasis, kuriame sauguma mygtuko busena
uint8_t UzdarymasLaukiu=0; // MygtukasUzdaryti kintamasis, kuriame saugoma mygtuko busena
uint8_t AtidarytaLaukiu=0; // DaviklisAtidaryta kintamasis, kuriame saugoma mygtuko busena
uint8_t DaviklisJudesysBusena=0; //DaviklisJudesysLaukiu, kuriame saugoma lazerio ir fotorezistoriaus busena 
uint8_t AtidarymasLaukiuBluetooth=0;//kintamasis, kuriame saugoma is bluetooth gauta info
uint8_t UzdarymasLaukiuBluetooth=0; // kintamasis, kuriame saugoma is bluetooth gauta info

uint8_t DaviklisAtidarytaLaukiu=0; //  DaviklisAtidarytaLaukiu kintamasis, kuriame saugoma daviklio busena
uint8_t DaviklisUzdarytaLaukiu=0; //  DaviklisUzdarytaLaukiu kintamasis, kuriame saugoma daviklio busena
uint8_t Keitimas=0; // kintamasis kad galima butu padaryti su case'ais
uint8_t StabdymasLaukiuMAtidarymas=0;// stabdymo kintamasis, kad galima butu sustabdyti su atdidarymo mygtuku
uint8_t StabdymasLaukiuMUzdarymas=0;//stabdymo kintamasis, kad galima butu sustabdyti su uzdarymo mygtuku

uint8_t StabdymasLaukiuBluetoothAtidarymas=0;//stabdymo kintamasis, kad galima butu sustabdyti per bluetooth
uint8_t StabdymasLaukiuBluetoothUzdarymas=0;// stabdymo kintamasis, kad galima butu sustabdyti per bluetootj

uint8_t AtidarymoReleBusena; // kintamasis atidarymo reles busenai nustatyti.
uint8_t UzdarymoReleBusena; //kintamasis uzdarymo reles busenai nustatyti



uint8_t SpindulysDaviklisJudesysBusena; // kintamasis spindulio relei



uint8_t DaviklisAtidarytaBusena;// kintamasis atidarymo daviklio busenai nustatyti
uint8_t DaviklisUzdarytaBusena; // kintamasis uzdarymo daviklio busenai nustatyti
const uint8_t HIGH=1; // mygtuko padeciai nustatyti(mygtukas nuspaustas)
const uint8_t LOW=0; //mygtyko padeciai nustatyti(mygtukas neispaustas)
const uint8_t ReleHIGH=0; // reles padeciai nustatyti ar ji ijungta, ar isjungta (cia rele ijungta)
const uint8_t ReleLOW=1;//reles padeciai nustatyti ar ji ijungta, ar isjungta (cia rele isjungta)
uint8_t keitimas=0; // tam, kad galima butu padaryti su case'ais
uint8_t BluetoothKintamasis=0;


#endif //_KINTAMIEJI_H_
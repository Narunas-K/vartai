#ifndef _FUNKCIJOS_H_
#define _FUNKCIJOS_H_

#include "stm32f4xx.h"
#include "stdio.h"
void StabdymasFunkcija(void); // stabdymo funkcija
void UzdarymasFunkcija(void); // vartu uzdarymo proceduros pirmoji funkcija
void VartaiUzdaromi(void); // vartu uzdarymo proceduros antroji funkcija
void UzdarymasDavikliai(void);// Davikliu funkcija uzdarymo funkcijai
void AtidarymasFunkcija(void); // vartu atidarymo proceduros pirmoji funkcija
void VartaiAtidaromi(void); // vartu atidarymo proceduros antroji funkcija
void AtidarymasDavikliai(void);// Davikliu funkcija atidarymo funkcijai
uint8_t Bluetooth_Gauti(void); // funkcija reikalinga informacijai is usart2 per bluetooth gauti
void delay(uint32_t ms); // delay funkcijai (netobulas delay'ius, reiks normalaus)!!!!!!!!!
void Delay (uint32_t dlyTicks);
//void adc_configure(void);//ADC konverterio, reikalingo ,,akiai'', inicializacija
//int adc_convert(void); //ADC konverterio funkcija nuskaityti duomenis is pino


#endif //_FUNKCIJOS_H_